{
  description = "Smart contracts of GitDAO";

  inputs = {
    devshell.url = "github:numtide/devshell";
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = { self, devshell, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system: 
      let

        pkgs = import nixpkgs {
            inherit system;
            overlays = [ devshell.overlay ];
        };

        packageJSON = ./package.json;
        package = pkgs.lib.importJSON packageJSON;

        pname = package.name;
        version = package.version;

        src = builtins.path {
          path = ./.;
          name = pname;
        };
      in {

        devShell = # Used by `nix develop`, value needs to be a derivation
          pkgs.devshell.mkShell {

            name = "GitDao";

            commands = [
              # Custom Commands
              {
                name = "hh";
                help = "Hardhat shortcut";
                # category = "CI";
                command = ''
                  npx hardhat "$@"
                '';
              }
              # Package Imports
              {
                category = "javascript";
                package = pkgs.nodePackages.pnpm;
              }
            ];

            packages = with pkgs; [
              nodejs
            ];
          };
      }
  );
}
