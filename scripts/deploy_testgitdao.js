// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");

const SECONDS_PER_HOUR = 3600;
const SECONDS_PER_YEAR = 365 * 24 * SECONDS_PER_HOUR;

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  const LinearGitDao = await hre.ethers.getContractFactory("LinearGitDao");
  const testGitDao = await LinearGitDao.deploy(
    "Test GitDao",
    48 * SECONDS_PER_HOUR,
    30, // 24 * SECONDS_PER_HOUR,
    60 * 5,
    48 * SECONDS_PER_HOUR
  );

  await testGitDao.deployed();

  console.info(`TestGitDao contract deployed at address ${testGitDao.address}`);

  // await (await testGitDao.createProposal("0x1234567890")).wait()

  // const accounts = await ethers.getSigners();
  // const myAccount = accounts[19];

  // await (await testGitDao.mint(myAccount.address)).wait();
  // await (await testGitDao.mint(myAccount.address)).wait();
  // await (await testGitDao.mint(myAccount.address)).wait();
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
