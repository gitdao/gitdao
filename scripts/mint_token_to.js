const hre = require("hardhat");

async function main() {
  [alice, bob, charlie] = await hre.ethers.getSigners();

  await alice.sendTransaction({
    to: hre.ethers.utils.getAddress("0x38B77d68943368D9bB188d463d1fea880D48868C"),
    value: hre.ethers.utils.parseEther("1.0")
  })
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
