// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/math/Math.sol";

import "./TimeLimitedGovernanceToken.sol";

// TODO Fire events when Governance Token are minted.

/// @title Provides a governance token whose power decreases linearly in time.
contract LinearlyTimeLimitedGovernanceToken is TimeLimitedGovernanceToken {

    //-------------------------------------------------------------------------
    // State
    //-------------------------------------------------------------------------

    /// @notice The time it takes since mint time for a token to reach its
    ///  asymptotic power.
    /// @dev Given in seconds since mint instant.
    uint256 durationToAsymptoticPowerInSeconds;

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------

    /// @notice Constructs a linearly decreasing governance token.
    /// @param _durationToAsymptoticPowerInSeconds The time it will take for a
    ///  given governance token to go from its full power to its asymptotic
    ///  power.
    constructor(uint256 _durationToAsymptoticPowerInSeconds) {
        durationToAsymptoticPowerInSeconds = _durationToAsymptoticPowerInSeconds;
    }

    //-------------------------------------------------------------------------
    // External Functions
    //-------------------------------------------------------------------------

    // TODO Protect the mint function with the `onlyAdministrator` modifier.
    /// @notice Mints a token to `_to` with a decreasing power set to 100 and an
    ///  asymptotic power set to 100.
    /// @param _to The address to which the token should be minted.
    function mint(
        address _to
    )
        external
        returns(uint256)
    {
        return _mint(_to, 100, 100, durationToAsymptoticPowerInSeconds / 2);
    }

    //-------------------------------------------------------------------------
    // Internal Functions
    //-------------------------------------------------------------------------

    /// @inheritdoc TimeLimitedGovernanceToken
    function _getVariablePowerOfToken(
        TimeLimitedGovernanceTokenMetadata memory token
    )
        internal
        view
        override
        returns (uint256)
    {
        uint256 durationLeftUntilAsymptoticPower;

        uint256 elapsedTimeSinceMintInstantInSeconds = block.timestamp -
            token.mintInstantInSeconds;

        if (elapsedTimeSinceMintInstantInSeconds >= durationToAsymptoticPowerInSeconds) {
            durationLeftUntilAsymptoticPower = 0;
        } else {
            durationLeftUntilAsymptoticPower = durationToAsymptoticPowerInSeconds
                - elapsedTimeSinceMintInstantInSeconds;
        }

        uint256 remainingPower = token.decreasingPower *
            durationLeftUntilAsymptoticPower /
            durationToAsymptoticPowerInSeconds;

        return remainingPower;
    }

    //-------------------------------------------------------------------------
    // Private Functions
    //-------------------------------------------------------------------------
}