// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/extensions/IERC721Metadata.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

import "./NonTransferableERC721.sol";

/// @title A non-transferable ERC721 token that implements the Metadata
///  extension.
///  See https://eips.ethereum.org/EIPS/eip-721
/// @author Yves Zumbach <yves@zumbach.dev>
contract NonTransferableERC721WithMetadata is
    NonTransferableERC721,
    IERC721Metadata
{
    using Strings for uint256;

    //-------------------------------------------------------------------------
    // State
    //-------------------------------------------------------------------------

    /// @notice The name of the governance token.
    string private tokenName;

    /// @notice The symbol of the governance token.
    string private tokenSymbol;

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------

    /// @notice Constructs a non-transferable ERC721 contract with some
    ///  metadata.
    /// @param _tokenName A descriptive name for a collection of NFTs in this
    ///  contract.
    /// @param _tokenSymbol An abbreviated name for NFTs in this contract.
    constructor(
        string memory _tokenName,
        string memory _tokenSymbol
    ) {
        tokenName = _tokenName;
        tokenSymbol = _tokenSymbol;
    }

    //-------------------------------------------------------------------------
    // External Functions
    //-------------------------------------------------------------------------

    /// @notice A descriptive name for a collection of NFTs in this contract.
    function name()
        external
        view
        override
        returns (string memory)
    {
        return tokenName;
    }

    /// @notice Query if a contract implements an interface.
    ///  See https://eips.ethereum.org/EIPS/eip-165
    /// @param _interfaceId The interface identifier, as specified in ERC-165
    /// @dev Interface identification is specified in ERC-165. This function
    ///  uses less than 30,000 gas.
    /// @return `true` if the contract implements `interfaceID` and
    ///  `interfaceID` is not 0xffffffff, `false` otherwise
    function supportsInterface(
        bytes4 _interfaceId
    )
        public
        view
        override(NonTransferableERC721, IERC165)
        returns (bool)
    {
        return
            _interfaceId == type(IERC721Metadata).interfaceId ||
            super.supportsInterface(_interfaceId);
    }

    /// @notice An abbreviated name for NFTs in this contract.
    function symbol()
        external
        view
        override
        returns (string memory)
    {
        return tokenSymbol;
    }

    /// @notice A distinct Uniform Resource Identifier (URI) for a given asset.
    /// @dev Throws if `_tokenId` is not a valid NFT. URIs are defined in RFC
    ///  3986. The URI may point to a JSON file that conforms to the "ERC721
    ///  Metadata JSON Schema".
    function tokenURI(
        uint256 _tokenId
    )
        external
        view
        override
        onlyIfTokenExists(_tokenId)
        returns (string memory)
    {
        string memory baseURI = _baseURI();
        return bytes(baseURI).length > 0 ?
            string(abi.encodePacked(baseURI, _tokenId.toString())) :
            "";
    }

    //-------------------------------------------------------------------------
    // Internal Functions
    //-------------------------------------------------------------------------

    /// @notice Base URI for computing {tokenURI}.
    /// @dev If set, the resulting URI for each token will be the concatenation
    ///  of the `baseURI` and the `tokenId`. Empty by default, can be
    ///  overridden in child contracts.
    function _baseURI()
        internal
        view
        virtual
        returns (string memory)
    {
        return "";
    }
}