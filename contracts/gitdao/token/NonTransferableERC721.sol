// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/introspection/ERC165.sol";

/// @title A non-transferable ERC721 token.
/// @author Yves Zumbach <yves@zumbach.dev>
contract NonTransferableERC721 is ERC165, IERC721 {
    using Address for address;

    //-------------------------------------------------------------------------
    // State
    //-------------------------------------------------------------------------

    /// @notice A mapping from token UID to the address of its owner.
    mapping(uint256 => address) ownerOfToken;

    /// @notice A mapping from address to the number of tokens owned by this
    ///  address.
    mapping(address => uint256) countOfTokenOwnedBy;

    /// @notice Always make a function throw.
    modifier alwaysThrows(string memory _errorMessage) {
        require(true == false, _errorMessage);
        _;
    }

    //-------------------------------------------------------------------------
    // Modifiers
    //-------------------------------------------------------------------------

    /// @notice Checks that the provided token UID is the UID of a token that
    ///  does exist.
    /// @param _tokenId The UID of the token to check for existence.
    modifier onlyIfTokenExists(uint256 _tokenId) {
        require(
            _tokenExists(_tokenId),
            "NonTransferableERC721: Query for non-existing token"
        );
        _;
    }

    /// @notice Throws unless the message sender is the owner of the provided
    ///  token UID.
    /// @param _tokenId The UID of the token for which to check ownership.
    modifier onlyOwnerOfToken(uint256 _tokenId) {
        require(
            msg.sender == ownerOfToken[_tokenId],
            "NonTransferableERC721: Only the owner of the token is allowed to perform this action"
        );
        _;
    }

    //-------------------------------------------------------------------------
    // Public Functions
    //-------------------------------------------------------------------------

    /// @notice Query if a contract implements an interface.
    ///  See https://eips.ethereum.org/EIPS/eip-165
    /// @param _interfaceId The interface identifier, as specified in ERC-165
    /// @dev Interface identification is specified in ERC-165. This function
    ///  uses less than 30,000 gas.
    /// @return `true` if the contract implements `interfaceID` and
    ///  `interfaceID` is not 0xffffffff, `false` otherwise
    function supportsInterface(
        bytes4 _interfaceId
    )
        public
        view
        virtual
        override(ERC165, IERC165)
        returns (bool)
    {
        return
            _interfaceId == type(IERC721).interfaceId ||
            super.supportsInterface(_interfaceId);
    }

    //-------------------------------------------------------------------------
    // External Function
    //-------------------------------------------------------------------------

    /// @notice This action is not allowed for non-transferable token, hence it
    ///  will always throw. It is provided as a way to keep full compliance
    ///  with the ERC721 interface.
    /// @dev This function will always throw.
    /// @param _approved The new approved NFT controller
    /// @param _tokenId The NFT to approve
    function approve(
        address _approved,
        uint256 _tokenId
    )
        external
        override
        alwaysThrows(
            "NonTransferableERC721: approving is illegal for non-transferable token"
        )
    {}

    /// @notice This action is not allowed for non-transferable token, hence it
    ///  will always throw. It is provided as a way to keep full compliance
    ///  with the ERC721 interface.
    /// @dev This function will always throw.
    /// @param _operator Address to add to the set of authorized operators.
    /// @param _approved True if the operator is approved, false to revoke
    ///  approval.
    function setApprovalForAll(
        address _operator,
        bool _approved
    )
        external
        override
        alwaysThrows(
            "NonTransferableERC721: Approving for all is illegal for non-transferable token"
        )
    {}

    /// @notice This action is not allowed for non-transferable token, hence it
    ///  will always throw. It is provided as a way to keep full compliance
    ///  with the ERC721 interface.
    /// @dev This function will always throw.
    /// @param _from The current owner of the NFT.
    /// @param _to The new owner.
    /// @param _tokenId The NFT to transfer.
    /// @param data Additional data with no specified format, sent in call to
    ///  `_to`
    function safeTransferFrom(
        address _from,
        address _to,
        uint256 _tokenId,
        bytes memory data
    )
        external
        override
        alwaysThrows(
            "NonTransferableERC721: Safe transfering is illegal for non-transferable token"
        )
    {}

    /// @notice This action is not allowed for non-transferable token, hence it
    ///  will always throw. It is provided as a way to keep full compliance
    ///  with the ERC721 interface.
    /// @dev This function will always throw.
    /// @param _from The current owner of the NFT.
    /// @param _to The new owner.
    /// @param _tokenId The NFT to transfer.
    function safeTransferFrom(
        address _from,
        address _to,
        uint256 _tokenId
    )
        external
        override
        alwaysThrows(
            "NonTransferableERC721: Safe transfering is illegal for non-transferable token"
        )
    {}

    /// @notice This action is not allowed for non-transferable token, hence it
    ///  will always throw. It is provided as a way to keep full compliance
    ///  with the ERC721 interface.
    /// @dev This function will always throw.
    /// @param _from The current owner of the NFT.
    /// @param _to The new owner.
    /// @param _tokenId The NFT to transfer.
    function transferFrom(
        address _from,
        address _to,
        uint256 _tokenId
    )
        external
        override
        alwaysThrows(
            "NonTransferableERC721: Transfering is illegal for non-transferable token"
        )
    {}

    /// @notice Count all NFTs assigned to an owner
    /// @dev NFTs assigned to the zero address are considered invalid, and this
    ///  function throws for queries about the zero address.
    /// @param _owner An address for whom to query the balance
    /// @return The number of NFTs owned by `_owner`, possibly zero
    function balanceOf(
        address _owner
    )
        external
        view
        override
        returns (uint256)
    {
        require(
            _owner != address(0),
            "NonTransferableERC721: balanceOf the zero address is illegal"
        );
        return countOfTokenOwnedBy[_owner];
    }

    /// @notice Get the approved address for a single NFT. Non-transferable
    ///  token cannot be approved for any address. Therefore, this function
    ///  will always return the zero address.
    /// @dev Throws if `_tokenId` is not a valid NFT. Always return the zero
    ///  address.
    /// @param _tokenId The NFT to find the approved address for.
    /// @return The approved address for this NFT, or the zero address if there
    ///  is none.
    function getApproved(
        uint256 _tokenId
    )
        external
        view
        override
        onlyIfTokenExists(_tokenId)
        returns (address)
    {
        return address(0);
    }

    /// @notice Query if an address is an authorized operator for another
    /// address. Because operators are not allowed for non-transferable token,
    ///  this function will always return `false`.
    /// @param _owner The address that owns the NFTs.
    /// @param _operator The address that acts on behalf of the owner.
    /// @return True if `_operator` is an approved operator for `_owner`, false
    ///  otherwise
    function isApprovedForAll(
        address _owner,
        address _operator
    )
        external
        pure
        override
        returns (bool)
    {
        return false;
    }

    /// @notice Find the owner of an NFT
    /// @dev NFTs assigned to zero address are considered invalid, and queries
    ///  about them do throw.
    /// @param _tokenId The identifier for an NFT
    /// @return The address of the owner of the NFT
    function ownerOf(
        uint256 _tokenId
    )
        external
        view
        override
        onlyIfTokenExists(_tokenId)
        returns (address)
    {
        address _tokenOwner = ownerOfToken[_tokenId];
        return _tokenOwner;
    }

    //-------------------------------------------------------------------------
    // Internal functions
    //-------------------------------------------------------------------------

    /// @notice Destroys token with UID `_tokenId`.
    ///  This function will throw if no token with UID `_tokenId` exists.
    ///  Emits a {Transfer} event.
    /// @param _tokenId The UID of the token to burn.
    function _burn(
        uint256 _tokenId
    )
        internal
    {
        address _owner = ownerOfToken[_tokenId];

        // _beforeTokenTransfer(owner, address(0), tokenId);

        countOfTokenOwnedBy[_owner] -= 1;
        delete ownerOfToken[_tokenId];

        emit Transfer(_owner, address(0), _tokenId);

        // _afterTokenTransfer(owner, address(0), tokenId);
    }

    /// @notice Mints a token with UID `tokenId` and transfers it to `to`.
    ///  WARNING: Usage of this method is discouraged, use {_safeMint} whenever
    ///  possible.
    ///  Throws if `_tokenId` is a token UID already used or if `_to` is the
    ///  zero address.
    ///  Emits a {Transfer} event.
    /// @param _to The address of the owner of the token to mint.
    /// @param _tokenId The UID of the token to mint. The UID cannot be
    ///  already used by another token.
    function _mint(
        address _to,
        uint256 _tokenId
    )
        internal
    {
        require(
            _to != address(0),
            "NonTransferableERC721: minting to the zero address"
        );
        require(
            !_tokenExists(_tokenId),
            "NonTransferableERC721: token UID is already used"
        );

        // _beforeTokenTransfer(address(0), _to, _tokenId);

        countOfTokenOwnedBy[_to] += 1;
        ownerOfToken[_tokenId] = _to;

        emit Transfer(address(0), _to, _tokenId);

        // _afterTokenTransfer(address(0), _to, _tokenId);

        require(
            _checkOnERC721Received(address(0), _to, _tokenId, ""),
            "NonTransferableERC721: safe minting to non ERC721Receiver implementer."
        );
    }

    /// @notice Returns whether a token with UID `_tokenId` exists.
    /// @param _tokenId The UID of the token for which to check existence.
    /// @return True if the token with UID `_tokenId` exists, and false
    ///  otherwise.
    function _tokenExists(
        uint256 _tokenId
    )
        internal
        view
        returns (bool)
    {
        return ownerOfToken[_tokenId] != address(0);
    }

    //-------------------------------------------------------------------------
    // Private Functions
    //-------------------------------------------------------------------------

    // TODO Do we want to keep this for safe minting?
    /// @notice Internal function to invoke {IERC721Receiver-onERC721Received}
    /// on a target address.
    ///  The call is not executed if the target address is not a contract.
    /// @dev Function will throw if `from` is not the zero address. This is
    ///  because governance tokens cannot be transfered. The zero address
    ///  indicates a mint and this is valid. Any address different than the
    ///  zero address indicates a transfer and this is illegal.
    /// @param from address representing the previous owner of the given token
    ///  ID. Must be the zero address.
    /// @param to target address that will receive the tokens.
    /// @param tokenId uint256 ID of the token to be transferred.
    /// @param _data bytes optional data to send along with the call.
    /// @return bool whether the call correctly returned the expected magic
    ///  value.
    function _checkOnERC721Received(
        address from,
        address to,
        uint256 tokenId,
        bytes memory _data
    )
        private
        returns (bool)
    {
        require(
            from == address(0),
            "NonTransferableERC721: checkOnERC721Received with from set to an address different than the zero address"
        );
        if (to.isContract()) {
            try IERC721Receiver(to).onERC721Received(
                msg.sender,
                from,
                tokenId,
                _data
            ) returns (bytes4 retval) {
                return retval == IERC721Receiver.onERC721Received.selector;
            } catch (bytes memory reason) {
                if (reason.length == 0) {
                    revert(
                        "NonTransferableERC721: minting to non ERC721Receiver implementer"
                    );
                } else {
                    assembly {
                        revert(add(32, reason), mload(reason))
                    }
                }
            }
        } else {
            return true;
        }
    }
}
