// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Counters.sol";

import "./NonTransferableERC721.sol";

/// @title Implements an ERC721 governance token which is non-transferable.
///  This contract does not implement the rate at which the power of the
///  governance token decreases. This must be implemented in subclasses
///  inherit from this class.
abstract contract TimeLimitedGovernanceToken is NonTransferableERC721 {
    using Counters for Counters.Counter;

    //-------------------------------------------------------------------------
    // Structs
    //-------------------------------------------------------------------------

    /// @title A structure to keep the data of time-limited ERC721 governance
    ///  tokens.
    ///  The power of the token at time t is computed as the asymptotic power
    ///  plus the value by a function of the `decreasingPower` which must be
    ///  decreasing in time.
    /// @param decreasingPower The variable power of the token.
    /// @param asymptoticPower The power that the token will keep once the
    ///  decreasing power part is fully depleted.
    /// @param mintInstantInSeconds The unix timestamp of the instant at which
    ///  the token was minted.
    /// @param halfLifeInSeconds The duration in seconds after which the token
    ///  will have lost half of its power.
    struct TimeLimitedGovernanceTokenMetadata {
        uint256 decreasingPower;
        uint256 asymptoticPower;
        uint256 mintInstantInSeconds;
        uint256 halfLifeInSeconds;
    }

    //-------------------------------------------------------------------------
    // State
    //-------------------------------------------------------------------------

    /// @notice The address of the administrator of this governance token.
    ///  The administrator is the only one allowed to mint new governance
    ///  tokens.
    address administrator;

    /// @notice A mapping from token ID to the token's data.
    mapping(uint256 => TimeLimitedGovernanceTokenMetadata) tokens;

    /// @notice A counter used for the governance token UID.
    Counters.Counter private tokenIds;

    //-------------------------------------------------------------------------
    // Events
    //-------------------------------------------------------------------------

    /// @notice Emitted when a new time-limited governance token is minted.
    /// @param id The UID of the token.
    /// @param owner The address the token was minted to. Remember also that
    ///  governance token are not transferable.
    /// @param decreasingPower The part of the token's initial power that will
    ///  decrease over time.
    /// @param asymptoticPower The part of the token's initial power that will
    ///  not decrease over time.
    /// @param mintInstantInSeconds The unix timestamp of the moment the token
    ///  was minted.
    /// @param halfLifeInSeconds The duration in seconds after which the token
    ///  will have lost half of its power.
    event TimeLimitedGovernanceTokenMinted(
        uint256 indexed id,
        address indexed owner,
        uint256 decreasingPower,
        uint256 asymptoticPower,
        uint256 indexed mintInstantInSeconds,
        uint256 halfLifeInSeconds
    );

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------

    /// @notice Creates a new time-limited ERC721 governance token contract.
    ///  The address that instantiate this contract will be defined as its
    ///  administrator.
    constructor() {
        administrator = msg.sender;
    }

    //-------------------------------------------------------------------------
    // Modifiers
    //-------------------------------------------------------------------------

    /// @notice Throws unless the message sender is the administrator of this
    ///  contract.
    modifier onlyAdministrator(string memory _errorString) {
        require(msg.sender == administrator, _errorString);
        _;
    }

    //-------------------------------------------------------------------------
    // Public Functions
    //-------------------------------------------------------------------------

    // TODO Enable administrator to burn tokens too
    /// @notice Burns the provided token.
    ///  Throws if the token to burn does not exist. Throws if someone other
    ///  than the token's owner tries to burn the token.
    /// @param _tokenId The UID of the token to burn.
    function burn(
        uint256 _tokenId
    )
        public
        onlyIfTokenExists(_tokenId)
        onlyOwnerOfToken(_tokenId)
    {
        delete tokens[_tokenId];
        _burn(_tokenId);
    }

    /// @notice Returns the current power that a time-limited governance token
    ///  still grants. This function will throw if you query to power of a
    ///  token that does not exists.
    /// @param _tokenId The UID of the token whose current power should be
    ///  returned.
    function getCurrentPowerOfToken(
        uint256 _tokenId
    )
        public
        view
        onlyIfTokenExists(_tokenId)
        returns (uint256)
    {
        TimeLimitedGovernanceTokenMetadata memory token = tokens[_tokenId];
        return _getVariablePowerOfToken(token) + token.asymptoticPower;
    }

    /// @notice Computes the total power available in all the governance token
    ///  at this instant.
    ///  WARNING: This function might cost a lot of gas to execute!
    function totalPower()
        public
        view
        returns (uint256)
    {
        uint256 _totalPower = 0;
        for(uint i = 1; i <= tokenIds.current(); i++) {
            _totalPower += getCurrentPowerOfToken(i);
        }
        return _totalPower;
    }

    //-------------------------------------------------------------------------
    // External Functions
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    // Internal Functions
    //-------------------------------------------------------------------------

    /// @notice Returns the variable power that a time-limited governance token
    ///  still grants. It is expected that the power decreases over time, since
    ///  the mint instant.
    /// @param token The token whose variable power should be computed.
    function _getVariablePowerOfToken(
        TimeLimitedGovernanceTokenMetadata memory token
    )
        internal
        view
        virtual
        returns (uint256);

    /// @notice Mints a new governance token to `_to` with a power initially
    ///  set to `_decreasingPower + _asymptoticPower`.
    ///  Throws if not called by the contract's administrator. Throws if
    ///  `_to` is the zero address. Throws if the provided
    ///  `_decreasingPower` is smaller than zero. Throws if `_asymptoticPower`
    ///  is smaller than zero.
    /// @param _to The address to which the governance token should be minted.
    /// @param _decreasingPower The part of the initial token power that will
    ///  decrease over time.
    /// @param _asymptoticPower The part of the token's power that will never
    ///  decrease. At t == infinity, the token will have this power.
    /// @return The UID of the newly minted token.
    function _mint(
        address _to,
        uint256 _decreasingPower,
        uint256 _asymptoticPower,
        uint256 _halfLifeInSecond
    )
        internal
        returns (uint256)
    {
        require(
            _to != address(0),
            "TimeLimitedGovernanceToken: minting to the zero address"
        );
        require(
            _decreasingPower >= 0,
            "TimeLimitedGovernanceToken: minting a token with decreasing power < 0"
        );
        require(
            _asymptoticPower >= 0,
            "TimeLimitedGovernanceToken: minting a token with asymptotic power < 0"
        );
        require(
            _halfLifeInSecond > 0,
            "TimeLimitedGovernanceToken: half life of token <= 0"
        );

        tokenIds.increment();
        uint256 _newTokenId = tokenIds.current();
        tokens[_newTokenId] = TimeLimitedGovernanceTokenMetadata(
            _decreasingPower,
            _asymptoticPower,
            block.timestamp,
            _halfLifeInSecond
        );
        super._mint(_to, _newTokenId);
        emit TimeLimitedGovernanceTokenMinted(
            _newTokenId,
            _to,
            _decreasingPower,
            _asymptoticPower,
            block.timestamp,
            _halfLifeInSecond
        );

        return _newTokenId;
    }
}