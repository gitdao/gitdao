// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./GitDao.sol";
import "../token/LinearlyTimeLimitedGovernanceToken.sol";

// TODO Write documentation
contract LinearGitDao is GitDao, LinearlyTimeLimitedGovernanceToken {

    /// @notice Constructs a linear Git DAO.
    /// @param _projectName A name that describes the project this DAO applies
    ///  to.
    /// @param _challengeDurationInSeconds The duration in second of a
    ///  challenge from the moment it is triggered to the moment voting on the
    ///  challenges closes.
    /// @param _proposalCreatedToVotingOpenedDurationInSecond  The duration in
    ///  second between the instant a proposal is created and the instant voting
    ///  for this proposal opens.
    /// @param _votingOpenedDurationInSecond The duration in second that voting
    ///  for a proposal remains open.
    /// @param _durationToAsymptoticPowerInSeconds The time it will take for a
    ///  given governance token to go from its full power to its asymptotic
    ///  power.
    constructor(
        string memory _projectName,
        uint256 _challengeDurationInSeconds,
        uint256 _proposalCreatedToVotingOpenedDurationInSecond,
        uint256 _votingOpenedDurationInSecond,
        uint256 _durationToAsymptoticPowerInSeconds
    )
        GitDao(
            _projectName,
            _challengeDurationInSeconds,
            _proposalCreatedToVotingOpenedDurationInSecond,
            _votingOpenedDurationInSecond
        )
        LinearlyTimeLimitedGovernanceToken(
            _durationToAsymptoticPowerInSeconds
        )
    {}
}