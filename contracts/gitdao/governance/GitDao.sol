// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Counters.sol";

import "../token/LinearlyTimeLimitedGovernanceToken.sol";

// TODO Implement quadratic voting

// TODO Prevent blacklisted addresses from doing anything with the DAO

// TODO Write GitDAO documentation
abstract contract GitDao is TimeLimitedGovernanceToken {
    using Counters for Counters.Counter;

    //-------------------------------------------------------------------------
    // Enum
    //-------------------------------------------------------------------------

    /// @title All the states in which a proposal can be when it is
    ///  (optimistically) trusted.
    /// @param Pending The state a proposal is in after being created and
    ///  before voting opens.
    /// @param VotingOpened The state a proposal is in once the voting period
    ///  is opened and not yet finished.
    /// @param VotingElapsed The state a proposal is in once the voting period
    ///  is elapsed but no one finished the proposal yet.
    /// @param Succeeded The state a proposal ends up in after someone finishes
    ///  the proposal and the vote succeeded. This state is final.
    /// @param Defeated The state a proposal ends up in after someone finishes
    ///  the proposal and the vote failed. This state is final.
    /// @param Canceled The state a proposal finishes in when its proposer
    ///  decides to withdraw the proposal. This state is final.
    enum ProposalState {
        Pending,
        VotingOpened,
        VotingElapsed,
        Succeeded,
        Defeated,
        Canceled
    }

    /// @title The trust level that a proposal enjoys.
    /// @param OptimisticallyTrusted The default trust level.This indicates
    ///  that the proposal is trusted by defaut and noone saw a reason to
    ///  challenge it.
    /// @param Challenged A transient trust level. Indicates that at least
    ///  one person thinks that the proposal is adversarial.
    ///  At this trust level, the proposal is stalled and a voting is opened
    ///  to get the community's opinion on whether the proposal is indeed
    ///  adversarial.
    /// @param ChallengeElapsed A transient trust level achieved after the
    ///  duration of the challenge elapsed but before anyone finished the
    ///  challenge.
    /// @param Whitelisted Indicates a proposal that is considered not
    ///  adversarial. This state is final.
    /// @param BlackListed Indicates a proposal that is considered adversarial.
    ///  This state is final.
    enum TrustLevel {
        OptimisticallyTrusted,
        Challenged,
        ChallengeElapsed,
        Whitelisted,
        Blacklisted
    }

    //-------------------------------------------------------------------------
    // Struct
    //-------------------------------------------------------------------------

    /// @title An on-chain agreement between the owners of a Radicle Org about
    ///  the state of a project's source code.
    /// @notice Anchors are a secure and verifiable way to keep track of the
    ///  canonical state of a repository.
    ///  See https://docs.radicle.xyz/docs/connecting-to-ethereum/anchoring-projects
    /// @param tag A tag that can be used to discriminate between anchor types.
    /// @param multihash The hash being anchored,encoded in multihash format.
    struct Anchor {
        uint32 tag;
        bytes multihash;
    }

    /// @title Store all information related to a challenge. Challenges are
    ///  indexed by the UID of the proposal they challenge.
    /// @dev As there can be only once challenge per proposal, this works.
    /// @param stopInstant As soon as a challenge is requested, the
    ///  challenge voting period is opened. This field contains the unix
    ///  timestamp at which the voting period of the challenge closes.
    /// @param yesPower The amount of power in favor of the challenge.
    ///  When voting "yes" using a time-limited governance token, the
    ///  power of the token at the instant of voting is added to this field.
    /// @param noPower The amount of power against the challenge.
    ///  When voting "no" using a time-limited governance token, the
    ///  power of the token at the instant of voting is added to this field.
    /// @param tokenWasUsed A mapping from the governance token UID to a
    ///  boolean indicating whether the token was already used to vote on
    ///  this challenge.
    /// @param IdOfTokensUsedForYes An array containing all the tokens used
    ///  to vote "yes" on the challenge.
    /// @param IdOfTokensUsedForNo An array containing all the tokens used
    ///  to vote "no" on the challenge.
    struct Challenge {
        uint256 stopInstant;
        uint256 yesPower;
        uint256 noPower;
        mapping (uint256 => bool) tokenWasUsed;
        uint256[] IdOfTokensUsedForYes;
        uint256[] IdOfTokensUsedForNo;
    }

    // TODO Should we add a URI field to proposal that gives the off-chain location where the proposal is described? I.e. where does Radicle store comments of a proposal?
    /// @title A data structure to keep all information related to a given
    ///  proposal.
    /// @param proposer The address of the account that is proposing.
    /// @param trustLevel The trust level that the proposal currently enjoys.
    /// @param state The state of the proposal.
    /// @param commitMultihash The multihash of the proposed commit.
    /// @param votingStartInstant The unix timestamp at which the vote will
    ///  open.
    /// @param votingStopInstant The unix timestamp at which the vote will
    ///  close.
    /// @param yesPower The amount of power in favor of the proposal.
    ///  When voting "yes" using a time-limited governance token, the
    ///  power of the token at the instant of voting is added to this field.
    /// @param noPower The amount of power against the proposal.
    ///  When voting "no" using a time-limited governance token, the
    ///  power of the token at the instant of voting is added to this field.
    /// @param tokenWasUsed A mapping from the governance token UID to a
    ///  boolean indicating whether the token was already used to vote.
    struct Proposal {
        address proposer;
        TrustLevel trustLevel;
        ProposalState state;
        bytes commitMultihash;
        uint256 votingStartInstant;
        uint256 votingStopInstant;
        uint256 yesPower;
        uint256 noPower;
        mapping (uint256 => bool) tokenWasUsed;
    }

    //-------------------------------------------------------------------------
    // State
    //-------------------------------------------------------------------------

    /// @notice The list of addresses that are not allowed anymore to create
    ///  proposals to this DAO.
    mapping(address => bool) public blacklistedAddresses;

    /// @notice A mapping from proposal UID to the proposal's challenge if
    ///  if there is one.
    mapping(uint256 => Challenge) public challenges;

    /// @notice The anchor to the commit that is currently endorsed by this
    ///  project. To get the list of commit that this DAO endorsed, have a look
    ///  at the events emitted each time a new commit is endorsed.
    Anchor public currentlyEndorsedCommit;

    /// @notice A name that describes the project this DAO applies to.
    string public projectName;

    /// @notice The duration in second between the instant a proposal is
    ///  created and the instant voting for this proposal opens.
    uint256 public proposalCreatedToVotingOpenedDurationInSeconds;

    /// @notice A counter to create proposal UIDs.
    Counters.Counter private proposalIds;

    /// @notice A mapping from proposal UID to the proposal's data.
    mapping(uint256 => Proposal) public proposals;

    /// @notice The duration in second of a challenge from the moment it is
    ///  triggered to the moment voting on the challenges closes.
    uint256 challengeDurationInSeconds;

    /// @notice The duration in second that voting for a proposal remains open.
    uint256 public votingOpenedDurationInSeconds;

    //-------------------------------------------------------------------------
    // Events
    //-------------------------------------------------------------------------

    /// @notice Emitted when a vote is cast on a challenge.
    /// @param voter The address that submitted the vote.
    /// @param proposalId The UID of the proposal the challenge applies to.
    /// @param tokenIds The UIDs of the token used for voting.
    /// @param power The power that this token provided when voting.
    /// @param isInFavor Whether the vote was casted in favor or against the
    ///  challenge.
    event ChallengeVoteCast(
        address indexed voter,
        uint256 indexed proposalId,
        uint256[] tokenIds,
        uint256 power,
        bool isInFavor
    );

    /// @notice Emitted when a challenge is finished and the challenge passed.
    ///  At this point, the corresponding proposal is blacklisted, i.e.
    ///  considered as adversarial.
    /// @param proposalId The UID of the proposal that got blacklisted.
    /// @param yesPower The quantity of power used to vote "yes".
    /// @param noPower The quantity of power used to vote "no".
    event ProposalBlacklisted(
        uint256 indexed proposalId,
        uint256 yesPower,
        uint noPower
    );

    /// @notice Emitted when a proposal is canceled.
    /// @param proposalId The UID of the canceled proposal.
    event ProposalCanceled(
        uint256 indexed proposalId
    );

    /// @notice Emitted when a given proposal is challenged, i.e. the
    ///  challenger considers the proposal as being adversarial.
    /// @param proposalId The UID of the proposal being challenged.
    /// @param challenger The address that opened the challenge.
    /// @param tokenIds The UIDs of the token used for the challenge.
    /// @param startInstant The unix timestamp at which the challenge
    ///  was started, i.e. the instant at which it was created.
    /// @param stopInstant The unix timestamp at which the
    ///  challenge stops.
    event ProposalChallenged(
        uint256 indexed proposalId,
        address challenger,
        uint256[] tokenIds,
        uint256 indexed startInstant,
        uint256 indexed stopInstant
    );

    /// @notice Emitted when a proposal is created.
    /// @param id The UID that the proposal was assigned.
    /// @param proposer The address that created the proposal.
    /// @param creationInstant The unix timestamp at which the proposal was
    ///  created.
    /// @param votingStartInstant The unix timestamp at which voting will start.
    /// @param votingStopInstant The unix timestamp at which voting will close.
    event ProposalCreated(
        uint256 indexed id,
        address indexed proposer,
        uint256 indexed creationInstant,
        bytes commitMultihash,
        uint256 votingStartInstant,
        uint256 votingStopInstant
    );

    /// @notice Emitted when someone finishes a proposal and that the proposal
    ///  successfully passed the vote. At this point, it is considered that
    ///  the commit proposed by the proposal is endorsed by the DAO, Congrats!
    /// @param id The UID of the proposal that was finished.
    /// @param commitMultihash The multihash of the commit that became endorsed
    ///  by this DAO.
    event ProposalSucceeded(
        uint256 indexed id,
        bytes commitMultihash
    );

    /// @notice Emitted when someone finishes a proposal and that the proposal
    ///  failed the vote.
    /// @param id The UID of the proposal that was finished.
    event ProposalDefeated(
        uint256 indexed id
    );

    /// @notice Emitted when a challenge is finished and the challenge failed.
    ///  At this point, the corresponding proposal is whitelisted, i.e.
    ///  considered as non-adversarial.
    /// @param proposalId The UID of the proposal that got whitelisted.
    /// @param yesPower The quantity of power used to vote "yes".
    /// @param noPower The quantity of power used to vote "no".
    event ProposalWhitelisted(
        uint256 indexed proposalId,
        uint256 yesPower,
        uint noPower
    );

    // TODO Add voting moment
    /// @notice Emitted when a vote is cast.
    /// @param voter The address that submitted the vote.
    /// @param proposalId The UID of the proposal the vote applies to.
    /// @param tokenIds The UIDs of the token used for voting.
    /// @param power The power that this token provided when voting.
    /// @param isInFavor Whether the vote was casted in favor or against the
    ///  proposal.
    event VoteCast(
        address indexed voter,
        uint256 indexed proposalId,
        uint256[] tokenIds,
        uint256 power,
        bool isInFavor
    );

    //-------------------------------------------------------------------------
    // Constructor
    //-------------------------------------------------------------------------

    /// @notice Constructs a Governance DAO based on time-limited governance
    ///   token.
    /// @param _projectName A name that describes the project this DAO applies
    ///  to.
    /// @param _challengeDurationInSeconds The duration in second of a
    ///  challenge from the moment it is triggered to the moment voting on the
    ///  challenges closes.
    /// @param _proposalCreatedToVotingOpenedDurationInSecond  The duration in
    ///  second between the instant a proposal is created and the instant voting
    ///  for this proposal opens.
    /// @param _votingOpenedDurationInSecond The duration in second that voting
    ///  for a proposal remains open.
    constructor(
        string memory _projectName,
        uint256 _challengeDurationInSeconds,
        uint256 _proposalCreatedToVotingOpenedDurationInSecond,
        uint256 _votingOpenedDurationInSecond
    ) {
        projectName = _projectName;
        challengeDurationInSeconds = _challengeDurationInSeconds;
        proposalCreatedToVotingOpenedDurationInSeconds = _proposalCreatedToVotingOpenedDurationInSecond;
        votingOpenedDurationInSeconds = _votingOpenedDurationInSecond;
    }

    //-------------------------------------------------------------------------
    // Modifiers
    //-------------------------------------------------------------------------

    /// @notice Checks that the provided proposal UID matches that of a
    ///  proposal that can be canceled, i.e. that the proposal is pending
    ///  and that the sender is the accoun that created the proposal.
    /// @param _proposalId The UID of the proposal for which to check
    ///  for cancelability.
    modifier onlyIfAllowedToCancel(uint256 _proposalId) {
        require(
            proposalIsTrusted(_proposalId)
                && getProposalState(_proposalId) == ProposalState.Pending
                && msg.sender == proposals[_proposalId].proposer,
            "GitDao: Proposal cannot be canceled."
        );
        _;
    }

    /// @notice Checks that the provided proposal UID matches that of a
    ///  proposal that is optimistically trusted.
    /// @param _proposalId The UID of the proposal for which to check
    ///  optimistically trustedness.
    modifier onlyIfAllowedToChallengeProposal(uint256 _proposalId) {
        require(
            getProposalTrustLevel(_proposalId) == TrustLevel.OptimisticallyTrusted
                && !_proposalHasReachedFinalState(_proposalId),
            "GitDao: Proposal cannot be challenged."
        );
        _;
    }

    /// @notice Checks that the challenge of the proposal matching the given
    ///  proposal UID can be finished.
    /// @param _proposalId The UID of the proposal for which to check that
    ///  its corresponding challenge can be finished.
    modifier onlyIfAllowedToFinishChallenge(
        uint256 _proposalId
    ) {
        require(
            getProposalTrustLevel(_proposalId) == TrustLevel.ChallengeElapsed,
            "GitDao: Challenge cannot be finished"
        );
        _;
    }

    /// @notice Checks that the proposal matching the given proposal UID
    ///  can be finished.
    /// @param _proposalId The UID of the proposal for which to check that
    ///  it can be finished.
    modifier onlyIfAllowedToFinishProposal(
        uint256 _proposalId
    ) {
        require(
            proposalIsTrusted(_proposalId)
                && getProposalState(_proposalId) == ProposalState.VotingElapsed,
            "GitDao: Proposal cannot be finished"
        );
        _;
    }

    /// @notice Checks that the voting is currently allowed.
    /// @param _proposalId The UID of the proposal for which to check that
    ///  voting is currently allowed.
    modifier onlyIfAllowedToVote(
        uint256 _proposalId
    ) {
        require(
            proposalIsTrusted(_proposalId)
                && getProposalState(_proposalId) == ProposalState.VotingOpened,
            "GitDao: Voting is not opened"
        );
        _;
    }

    /// @notice Checks that the provided proposal UID matches that of a
    ///  proposal that is currently being challenged.
    /// @param _proposalId The UID of the proposal for which to check
    ///  if there is a challenge opened now.
    modifier onlyIfAllowedToVoteOnChallenge(uint256 _proposalId) {
        require(
            getProposalTrustLevel(_proposalId) == TrustLevel.Challenged,
            "GitDao: Cannot vote on challenge."
        );
        _;
    }

    /// @notice Checks that `msg.sender` is not a blacklisted address.
    modifier onlyIfNotBlacklistedAddress() {
        require(
            blacklistedAddresses[msg.sender] != true,
            "GitDao: Request from blacklisted address"
        );
        _;
    }

    /// @notice Checks that the provided proposal UID matches that of a
    ///  proposal that does indeed exist.
    /// @param _proposalId The UID of the proposal whose existence should be
    ///  checked.
    modifier onlyIfProposalExists(uint256 _proposalId) {
        require(_proposalExists(_proposalId));
        _;
    }

    /// @notice Checks that the provided proposal UID matches that of a trusted
    ///  proposal.
    /// @param _proposalId The UID of the proposal for which to check
    ///  trustedness.
    modifier onlyIfProposalIsTrusted(uint256 _proposalId) {
        require(
            proposalIsTrusted(_proposalId),
            "GitDao: Proposal is not trusted."
        );
        _;
    }

    //-------------------------------------------------------------------------
    // Public Functions
    //-------------------------------------------------------------------------

    /// @notice Returns the state of the proposal matching the given proposal
    ///  UID. This function will always return the correct state, i.e. it will
    ///  update the state if needed, for example if some state should be
    ///  triggered based on timers.
    /// @param _proposalId The UID of the proposal whose state should be 
    ///  (updated and) retrieved.
    /// @return The state of the provided proposal.
    function getProposalState(
        uint256 _proposalId
    )
        public
        onlyIfProposalExists(_proposalId)
        returns(ProposalState)
    {
        Proposal storage _proposal = proposals[_proposalId];

        if (_proposalHasReachedFinalState(_proposalId)) {
            return _proposal.state;
        } else if (block.timestamp < _proposal.votingStartInstant) {
            _proposal.state = ProposalState.Pending;
        } else if (
            _proposal.votingStartInstant <= block.timestamp
                && block.timestamp < _proposal.votingStopInstant
        ) {
            _proposal.state = ProposalState.VotingOpened;
        } else {
            _proposal.state = ProposalState.VotingElapsed;
        }
        return _proposal.state;
    }

    /// @notice Returns the trust level of the proposal matching the given
    ///  proposal UID. This function will always return the correct trust
    ///  level, i.e. it will update the trust level if needed, for example
    ///  if some trust level should be triggered based on timers.
    /// @param _proposalId The UID of the proposal whose trust level should be
    ///  (updated and) retrieved.
    /// @return The trust level of the provided proposal.
    function getProposalTrustLevel(
        uint256 _proposalId
    )
        public
        onlyIfProposalExists(_proposalId)
        returns(TrustLevel)
    {
        Proposal storage _proposal = proposals[_proposalId];

        if (_proposalHasReachedFinalTrustLevel(_proposalId)) {
            return _proposal.trustLevel;
        } else if (_proposal.trustLevel == TrustLevel.OptimisticallyTrusted) {
            return _proposal.trustLevel;
        } else {
            Challenge storage _challenge = challenges[_proposalId];

            if (block.timestamp < _challenge.stopInstant) {
                _proposal.trustLevel = TrustLevel.Challenged;
            } else {
                _proposal.trustLevel = TrustLevel.ChallengeElapsed;
            }

            return _proposal.trustLevel;
        }
    }

    /// @notice Returns whether the proposal matching the given proposal UID
    ///  is trusted.
    /// @param _proposalId The UID of the proposal for which to check if it is
    ///  trusted.
    /// @return True if the proposal is trusted and false otherwise.
    function proposalIsTrusted(
        uint256 _proposalId
    )
        public
        onlyIfProposalExists(_proposalId)
        returns(bool)
    {
        TrustLevel _trustLevel = getProposalTrustLevel(_proposalId);

        return _trustLevel == TrustLevel.OptimisticallyTrusted
                || _trustLevel == TrustLevel.Whitelisted;
    }

    //-------------------------------------------------------------------------
    // External Functions
    //-------------------------------------------------------------------------

    /// @notice Cancels a proposal.
    ///  Can only be called on a proposal by the account that created the
    ///  proposal and only if the proposal is cancelable.
    /// @param _proposalId The UID of the proposal to cancel.
    function cancelProposal(
        uint256 _proposalId
    )
        external
        onlyIfAllowedToCancel(_proposalId)
    {
        proposals[_proposalId].state = ProposalState.Canceled;
        emit ProposalCanceled(_proposalId);
    }

    /// @notice Casts a vote on a challenge using one of the governance tokens
    ///  of this contract.
    ///  Throws if the given proposal UID does not match an existing proposal.
    ///  Throws if the given proposal UID is not the UID of a currently
    ///  challenged proposal.
    ///  Throws if any of the provided token is non-existent, or already used
    ///  to vote on the given challenge, or if the message sender is not the
    ///  owner of the token.
    /// @param _proposalId The UID of the proposal whose challenge should be
    ///  voted on.
    /// @param _tokenIds The UIDs of the token to use for voting.
    /// @param _isInFavor Set this boolean to true if you want to vote in favor
    ///  of the challenge, i.e. if you think the original proposal is
    ///  adversarial, and set it to false if you are against the challenge,
    ///  i.e. you do not think that the original proposal is adversarial.
    function castChallengeVote(
        uint256 _proposalId,
        uint256[] memory _tokenIds,
        bool _isInFavor
    )
        external
        onlyIfProposalExists(_proposalId)
        onlyIfAllowedToVoteOnChallenge(_proposalId)
    {
        uint256 _totalPower = 0;

        for (uint256 _i; _i < _tokenIds.length; ++_i) {
            uint256 _tokenId = _tokenIds[_i];

            // Check the given tokens.
            require(
                _tokenExists(_tokenId),
                "NonTransferableERC721: Query for non-existing token"
            );
            require(
                !challenges[_proposalId].tokenWasUsed[_tokenId],
                "GitDao: token was already used to vote on this challenge"
            );
            require(
                msg.sender == ownerOfToken[_tokenId],
                "NonTransferableERC721: Only the owner of the token is allowed to perform this action"
            );

            // Compute total power.
            uint256 _power = getCurrentPowerOfToken(_tokenId);
            _totalPower += _power;

            // Save token as used for this challenge.
            challenges[_proposalId].tokenWasUsed[_tokenId] = true;
            if (_isInFavor) {
                challenges[_proposalId].IdOfTokensUsedForYes.push(_tokenId);
            } else {
                challenges[_proposalId].IdOfTokensUsedForNo.push(_tokenId);
            }
        }

        if (_isInFavor) {
            challenges[_proposalId].yesPower += _totalPower;
        } else {
            challenges[_proposalId].noPower += _totalPower;
        }

        emit ChallengeVoteCast(
            msg.sender,
            _proposalId,
            _tokenIds,
            _totalPower,
            _isInFavor
        );
    }

    /// @notice Casts a vote on a proposal using one of the governance tokens
    ///  of this contract.
    ///  Throws if the given proposal UID does not match an existing proposal.
    ///  Throws if the voting period of the given proposal is not currently
    ///  opened.
    ///  Throws if any of the provided token is non-existent, or already used
    ///  to vote on the given proposal, or if the message sender is not the
    ///  owner of the token.
    /// @param _proposalId The UID of the proposal to vote on.
    /// @param _tokenIds The UIDs of the token to use for voting.
    /// @param _isInFavor Set this boolean to true if you want to vote in favor
    ///  of the proposal and set it to false if you are against the proposal.
    function castVote(
        uint256 _proposalId,
        uint256[] memory _tokenIds,
        bool _isInFavor
    )
        external
        onlyIfProposalExists(_proposalId)
        onlyIfProposalIsTrusted(_proposalId)
        onlyIfAllowedToVote(_proposalId)
    {
        Proposal storage _proposal = proposals[_proposalId];
        uint256 _totalPower = 0;

        for (uint256 _i; _i < _tokenIds.length; ++_i) {
            uint256 _tokenId = _tokenIds[_i];

            // Check the given tokens.
            require(
                _tokenExists(_tokenId),
                "NonTransferableERC721: Query for non-existing token"
            );
            require(
                !_proposal.tokenWasUsed[_tokenId],
                "GitDao: token was already used to vote on this proposal"
            );
            require(
                msg.sender == ownerOfToken[_tokenId],
                "NonTransferableERC721: Only the owner of the token is allowed to perform this action"
            );

            uint256 _power = getCurrentPowerOfToken(_tokenId);
            _totalPower += _power;
            _proposal.tokenWasUsed[_tokenId] = true;
        }

        if (_isInFavor) {
            _proposal.yesPower += _totalPower;
        } else {
            _proposal.noPower += _totalPower;
        }

        emit VoteCast(
            msg.sender,
            _proposalId,
            _tokenIds,
            _totalPower,
            _isInFavor
        );
    }

    // TODO Documentation of function challengeProposal
    function challengeProposal(
        uint256 _proposalId,
        uint256[] memory _tokenIds
    )
        external
        onlyIfProposalExists(_proposalId)
        onlyIfAllowedToChallengeProposal(_proposalId)
    {
        uint256 _challengeStopInstant =
            block.timestamp + challengeDurationInSeconds;

        uint256 _yesPower = 0;
        for (uint256 _i; _i < _tokenIds.length; ++_i) {
            uint256 _tokenId = _tokenIds[_i];

            // Check the given tokens.
            require(
                _tokenExists(_tokenId),
                "NonTransferableERC721: Query for non-existing token"
            );
            require(
                msg.sender == ownerOfToken[_tokenId],
                "NonTransferableERC721: Only the owner of the token is allowed to perform this action"
            );

            uint256 _power = getCurrentPowerOfToken(_tokenId);
            _yesPower += _power;
        }

        // Store the challenge values
        challenges[_proposalId].stopInstant = _challengeStopInstant;
        challenges[_proposalId].yesPower = _yesPower;

        // Save that some tokens were already used to vote on the challenge.
        for (uint256 _i; _i < _tokenIds.length; ++_i) {
            uint256 _tokenId = _tokenIds[_i];
            challenges[_proposalId].tokenWasUsed[_tokenId] = true;
            challenges[_proposalId].IdOfTokensUsedForYes.push(_tokenId);
        }

        // Update the proposal
        Proposal storage _proposal = proposals[_proposalId];
        _proposal.trustLevel = TrustLevel.Challenged;
        if (block.timestamp < _proposal.votingStartInstant) {
            _proposal.votingStartInstant = _proposal.votingStartInstant
                + challengeDurationInSeconds;
        }
        if (block.timestamp < _proposal.votingStopInstant) {
            _proposal.votingStopInstant = _proposal.votingStopInstant
                + challengeDurationInSeconds;
        }

        emit ProposalChallenged(
            _proposalId,
            msg.sender,
            _tokenIds,
            block.timestamp,
            _challengeStopInstant
        );
    }

    // TODO Require the proposer to lock some tokens in
    /// @notice Creates a new proposal.
    function createProposal(
        bytes memory _commitMultihash
    )
        external
        payable
        virtual
        onlyIfNotBlacklistedAddress
        returns(uint256)
    {
        proposalIds.increment();
        uint256 _newProposalId = proposalIds.current();

        address _proposer = msg.sender;

        uint256 _creationInstant = block.timestamp;

        uint256 _votingStartInstant = _creationInstant
            + proposalCreatedToVotingOpenedDurationInSeconds;

        uint256 _votingStopInstant = _votingStartInstant
            + votingOpenedDurationInSeconds;

        proposals[_newProposalId].proposer = _proposer;
        proposals[_newProposalId].state = ProposalState.Pending;
        proposals[_newProposalId].commitMultihash = _commitMultihash;
        proposals[_newProposalId].votingStartInstant = _votingStartInstant;
        proposals[_newProposalId].votingStopInstant = _votingStopInstant;

        emit ProposalCreated(
            _newProposalId,
            _proposer,
            _creationInstant,
            _commitMultihash,
            _votingStartInstant,
            _votingStopInstant
        );

        return _newProposalId;
    }

    /// @notice Finishes a challenge once its voting period is elapsed.
    ///  Throws if the given proposal UID does not match an existing proposal.
    ///  Throws if it is not allowed to finish the challenge, e.g. if the
    ///  voting period is not yet elapsed.
    /// @param _proposalId The UID of the proposal whose challenge should be
    ///  finished.
    /// @return True if the challenge was accepted, and false otherwise.
    function finishChallenge(
        uint256 _proposalId
    )
        external
        onlyIfProposalExists(_proposalId)
        onlyIfAllowedToFinishChallenge(_proposalId)
        returns(bool)
    {
        Challenge storage _challenge = challenges[_proposalId];

        if (_challenge.yesPower >= _challenge.noPower) {
            _succeedChallenge(_proposalId);
            return true;
        } else {
            _defeatChallenge(_proposalId);
            return false;
        }
    }

    /// @notice Finishes a proposal once its voting period is elapsed.
    ///  Throws if the given proposal UID does not match an existing proposal.
    ///  Throws if the voting period is not elapsed.
    /// @param _proposalId The UID of the proposal to finish.
    function finishProposal(
        uint256 _proposalId
    )
        external
        onlyIfProposalExists(_proposalId)
        onlyIfAllowedToFinishProposal(_proposalId)
        returns(bool)
    {
        Proposal storage _proposal = proposals[_proposalId];

        if (_proposal.noPower == 0) {
            // Optimistic voting procedure
            _succeedProposal(_proposalId);
            return true;
        } else {
            // Regular voting procedure.
            // TODO Check quorum?
            if (_proposal.yesPower >= _proposal.noPower) {
                _succeedProposal(_proposalId);
                return true;
            } else {
                _defeatProposal(_proposalId);
                return false;
            }
        }
    }

    //-------------------------------------------------------------------------
    // Internal Functions
    //-------------------------------------------------------------------------

    /// @notice Checks that a proposal does exist.
    /// @param _proposalId The UID of the proposal whose existence should be
    ///  checked.
    /// @return True if and only if the given proposal UID matches that of a
    ///  proposal that does exist.
    function _proposalExists(
        uint256 _proposalId
    )
        internal
        view
        returns(bool)
    {
        return proposals[_proposalId].proposer != address(0);
    }

    //-------------------------------------------------------------------------
    // Private Functions
    //-------------------------------------------------------------------------

    /// @notice Fails a challenge.
    /// @param _proposalId The UID of the proposal whose challenge should be
    ///  defeated.
    function _defeatChallenge(
        uint256 _proposalId
    )
        private
    {
        // Flag the proposal as white listed
        proposals[_proposalId].trustLevel = TrustLevel.Whitelisted;

        // Destroy all the governance tokens assigned to "yes"
        for (uint256 _i = 0; _i < challenges[_proposalId].IdOfTokensUsedForYes.length; ++_i) {
            burn(challenges[_proposalId].IdOfTokensUsedForYes[_i]);
        }

        emit ProposalWhitelisted(
            _proposalId,
            challenges[_proposalId].yesPower,
            challenges[_proposalId].noPower
        );
    }

    // TODO Give the locked tokens back to the proposer
    /// @notice Fails a proposal.
    /// @dev Sets the proposal state to defeated and emits the `ProposalDefeated`
    ///  event.
    /// @param _proposalId The UID of the proposal to fail.
    function _defeatProposal(
        uint256 _proposalId
    )
        private
    {
        proposals[_proposalId].state = ProposalState.Defeated;
        emit ProposalDefeated(
            _proposalId
        );
    }

    /// @notice Returns whether a proposal has reached a final state.
    ///  Once a proposal is in a final state, it cannot be modified anymore.
    ///  Once a proposal is in a final state it cannot be challenged either.
    /// @param _proposalId The UID of the proposal for which to check if
    ///  it is final.
    function _proposalHasReachedFinalState(
        uint256 _proposalId
    )
        private
        view
        onlyIfProposalExists(_proposalId)
        returns(bool)
    {
        return proposals[_proposalId].state == ProposalState.Succeeded
            || proposals[_proposalId].state == ProposalState.Defeated
            || proposals[_proposalId].state == ProposalState.Canceled;
    }

    /// @notice Returns whether a proposal has reached a final trust level.
    ///  Once a proposal is in a final trust level, its trust level cannot be
    ///  modified anymore.
    /// @param _proposalId The UID of the proposal for which to check if it
    ///  reached a final trust level.
    function _proposalHasReachedFinalTrustLevel(
        uint256 _proposalId
    )
        private
        view
        returns(bool)
    {
        return proposals[_proposalId].trustLevel == TrustLevel.Whitelisted
            || proposals[_proposalId].trustLevel == TrustLevel.Blacklisted;
    }

    // TODO Give the locked tokens back to the proposer
    /// @notice Makes a proposal successful.
    /// @dev Sets the state of the proposal to succeeded, set the anchor of
    ///  this DAO to the commit of the proposal, then emits the
    ///  `ProposalExecuted` event.
    /// @param _proposalId The UID of the proposal to make successful.
    function _succeedProposal(
        uint256 _proposalId
    )
        private
    {
        proposals[_proposalId].state = ProposalState.Succeeded;
        currentlyEndorsedCommit = Anchor(
            0,
            proposals[_proposalId].commitMultihash
        );
        emit ProposalSucceeded(
            _proposalId,
            proposals[_proposalId].commitMultihash
        );
    }

    /// @notice Makes a challenge successful, i.e. marks a proposal as
    ///  adversarial.
    /// @param _proposalId The UID of the proposal to mark as adversarial.
    function _succeedChallenge(
        uint256 _proposalId
    )
        private
    {
        proposals[_proposalId].trustLevel = TrustLevel.Blacklisted;

        // Burn all the governance tokens of those that voted against the
        // challenge
        for (uint256 _i = 0; _i < challenges[_proposalId].IdOfTokensUsedForNo.length; ++_i) {
            burn(challenges[_proposalId].IdOfTokensUsedForNo[_i]);
        }

        // Blacklist the proposal initiator
        blacklistedAddresses[proposals[_proposalId].proposer] = true;

        // TODO Burn all the tokens of the proposal initiator

        emit ProposalBlacklisted(
            _proposalId,
            challenges[_proposalId].yesPower,
            challenges[_proposalId].noPower
        );
    }

}