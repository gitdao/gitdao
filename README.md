# Basic Sample Hardhat Project

This project demonstrates a basic Hardhat use case. It comes with a sample contract, a test for that contract, a sample script that deploys that contract, and an example of a task implementation, which simply lists the available accounts.

Try running some of the following tasks:

```shell
npx hardhat accounts # Show the list of accounts configured for the default network
npx hardhat compile # Compiles the smart contracts
npx hardhat clean
npx hardhat test # Runs all the tests
npx hardhat node # Runs a local blockchain
npx hardhat help
```

## Deploy Smart Contract

```sh
# Start a local blockchain node on which to deploy the contracts
hh node

# In another terminal, run the deployment script
hh run --network localhost scripts/deploy_testgitdao.js
```

## Log of Deployed Contracts

- 2022-04-07T11:53: 0xc37A7552b31c78495Ad402b2B3847dF15903c5E4
