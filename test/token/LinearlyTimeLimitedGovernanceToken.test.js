const { expect } = require("chai");
const hre = require("hardhat");
const { ethers } = hre;

// See https://ethereum.stackexchange.com/questions/86633/time-dependent-tests-with-hardhat
// for the details on how to set time in the underlying test blockchain.

describe("LinearlyTimeLimitedGovernanceToken", () => {
  let alice;
  let bob;

  // The contract where transactions to the contract are sent from alice's address.
  let contractFromAlice;

  // The contract where transactions to the contract are sent from bob's address.
  let contractFromBob;

  beforeEach(async () => {
    [alice, bob] = await ethers.getSigners();

    const LinearlyTimeLimitedGovernanceToken = await ethers.getContractFactory(
      "LinearlyTimeLimitedGovernanceToken",
      alice);
    contractFromAlice = await LinearlyTimeLimitedGovernanceToken.deploy(10);
    contractFromBob = await contractFromAlice.connect(bob);
  });

  it("should return the correct current power of a token", async () => {
    // We add 5 to be certain that the value is in the future
    let now = Math.floor(Date.now() / 1000) + 5
    await network.provider.send("evm_setNextBlockTimestamp", [now])
    await (await contractFromAlice.mint(bob.address)).wait();

    expect(await contractFromBob.getCurrentPowerOfToken(1)).to.equal(200);

    await network.provider.send("evm_mine", [now + 5])
    expect(await contractFromBob.getCurrentPowerOfToken(1)).to.equal(150);

    await network.provider.send("evm_mine", [now + 10])
    expect(await contractFromBob.getCurrentPowerOfToken(1)).to.equal(100);

    await network.provider.send("evm_mine", [now + 15])
    expect(await contractFromBob.getCurrentPowerOfToken(1)).to.equal(100);
  });

  it("should return the correct total power", async () => {
    // We add 30 to be certain that the value is in the future
    let now = Math.floor(Date.now() / 1000) + 30
    await network.provider.send("evm_setNextBlockTimestamp", [now])
    await (await contractFromAlice.mint(bob.address)).wait();

    expect(await contractFromBob.totalPower()).to.equal(200);

    await network.provider.send("evm_mine", [now + 2])
    expect(await contractFromBob.totalPower()).to.equal(180);

    await network.provider.send("evm_mine", [now + 4])
    contractFromAlice.mint(bob.address);
    expect(await contractFromBob.totalPower()).to.equal(350); // Not sure why time advances by one more second 🤔
  });
});
