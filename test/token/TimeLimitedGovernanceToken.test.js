const { expect } = require("chai");
const { ethers, network } = require("hardhat");

describe("TimeLimitedGovernanceToken", () => {
  let alice;
  let bob;

  // The contract where transactions to the contract are sent from alice's address.
  let contractFromAlice;

  // The contract where transactions to the contract are sent from bob's address.
  let contractFromBob;

  let mintTransaction;
  let receiptOfMintTransaction;

  beforeEach(async () => {
    [alice, bob] = await ethers.getSigners();

    const LinearlyTimeLimitedGovernanceToken = await ethers.getContractFactory("LinearlyTimeLimitedGovernanceToken", alice);

    contractFromAlice = await LinearlyTimeLimitedGovernanceToken.deploy(10);

    contractFromBob = await contractFromAlice.connect(bob);

    mintTransaction = await contractFromAlice.mint(bob.address);
    receiptOfMintTransaction = await mintTransaction.wait();
  });

  it("should mint correctly", async () => {
    expect(receiptOfMintTransaction.events[0].event).to.equal("Transfer");
    expect(receiptOfMintTransaction.events[0].args[0]).to.equal(ethers.constants.AddressZero);
    expect(receiptOfMintTransaction.events[0].args[1]).to.equal(bob.address);
    expect(receiptOfMintTransaction.events[0].args[2]).to.equal(1);

    mintTransaction2 = await contractFromAlice.mint(alice.address);
    receiptOfMintTransaction2 = await mintTransaction2.wait();

    expect(receiptOfMintTransaction2.events[0].event).to.equal("Transfer");
    expect(receiptOfMintTransaction2.events[0].args[0]).to.equal(ethers.constants.AddressZero);
    expect(receiptOfMintTransaction2.events[0].args[1]).to.equal(alice.address);
    expect(receiptOfMintTransaction2.events[0].args[2]).to.equal(2);
  });

  it("should not allow minting from non-administrator", async () => {
    // Bob is not the administrator of the smart contract: Alice is.
    // Therefore, Bob should not be allowed to mint tokens.
    expect(contractFromBob.mint(alice.address)).to.be.revertedWith(
      "TimeLimitedGovernanceToken: Only administrator is allowed to mint tokens"
    );
  });

  it("should not allow non-owner of a token to burn the token", async () => {
    expect(contractFromAlice.burn(1)).to.be.revertedWith(
      "NonTransferableERC721: Only the owner of the token is allowed to perform this action"
    );
  });

  it("should fail when trying to burn a non-existing token", async () => {
    expect(contractFromBob.burn(2)).to.be.revertedWith(
      "NonTransferableERC721: Query for non-existing token"
    );
  });

  it("should burn tokens successfully", async () => {
    await contractFromBob.burn(1);
    expect(contractFromBob.getCurrentPowerOfToken(1)).to.be.revertedWith(
      "NonTransferableERC721: Query for non-existing token"
    );
  });
});
