const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("NonTransferableERC721", () => {
  let alice;
  let bob;

  // The contract where transactions to the contract are sent from alice's address.
  let contractFromAlice;

  // The contract where transactions to the contract are sent from bob's address.
  let contractFromBob;

  let mintTransaction;
  let receiptOfMintTransaction;

  beforeEach(async () => {
    [alice, bob] = await ethers.getSigners();

    const LinearlyTimeLimitedGovernanceToken = await ethers.getContractFactory("LinearlyTimeLimitedGovernanceToken", alice);

    contractFromAlice = await LinearlyTimeLimitedGovernanceToken.deploy(10);

    contractFromBob = await contractFromAlice.connect(bob);

    mintTransaction = await contractFromAlice.mint(bob.address);
    receiptOfMintTransaction = await mintTransaction.wait();
  });

  it("should not allow approving", async () => {
    expect(contractFromBob.approve(alice.address, 1)).to.be.revertedWith(
      "NonTransferableERC721: approving is illegal for non-transferable token"
    );
  });

  it("should not allow approving for all", async () => {
    expect(contractFromBob.setApprovalForAll(alice.address, 1)).to.be.revertedWith(
      "NonTransferableERC721: Approving for all is illegal for non-transferable token"
    );
  });

  it("should not allow safe transfering", async () => {
    expect(contractFromBob["safeTransferFrom(address,address,uint256)"](bob.address, alice.address, 1)).to.be.revertedWith(
      "NonTransferableERC721: Safe transfering is illegal for non-transferable token"
    )
    expect(contractFromBob["safeTransferFrom(address,address,uint256,bytes)"](bob.address, alice.address, 1, ethers.utils.toUtf8Bytes("reason"))).to.be.revertedWith(
      "NonTransferableERC721: Safe transfering is illegal for non-transferable token"
    );
  });

  it("should not allow transfering", async () => {
    expect(contractFromBob.transferFrom(bob.address, alice.address, 1)).to.be.revertedWith(
      "NonTransferableERC721: Safe transfering is illegal for non-transferable token"
    );
  });

  it("should return the correct balanceOf", async () => {
    expect(await contractFromBob.balanceOf(bob.address)).to.equal(1);
    expect(await contractFromBob.balanceOf(alice.address)).to.equal(0);

    await (await contractFromAlice.mint(alice.address)).wait();

    expect(await contractFromBob.balanceOf(bob.address)).to.equal(1);
    expect(await contractFromBob.balanceOf(alice.address)).to.equal(1);
  });

  it("should throw when querying the approved addresses of non-existing tokens", async () => {
    expect(contractFromBob.getApproved(0)).to.be.revertedWith(
      "NonTransferableERC721: Query for non-existing token"
    );
    expect(contractFromBob.getApproved(2)).to.be.revertedWith(
      "NonTransferableERC721: Query for non-existing token"
    );
  });

  it("should return the zero address when querying the approved addresses of a token", async () => {
    expect(await contractFromBob.getApproved(1)).to.equal(ethers.constants.AddressZero);
  });

  it("should return false when querying for authorized operators", async () => {
    expect(await contractFromBob.isApprovedForAll(alice.address, bob.address)).to.equal(false);
    expect(await contractFromBob.isApprovedForAll(bob.address, alice.address)).to.equal(false);
  });
});
