const { expect } = require("chai");
const hre = require("hardhat");
const {BigNumber} = require("ethers");
const { ethers } = hre;

describe("LinearGitDao", () => {
  let alice;
  let bob;

  // The contract where transactions to the contract are sent from alice's address.
  let contractFromAlice;
  let aliceGovernanceTokenId;
  let aliceGovernanceTokenId2;
  let aliceGovernanceTokenId3;

  // The contract where transactions to the contract are sent from bob's address.
  let contractFromBob;
  let bobGovernanceTokenId;
  let bobGovernanceTokenId2;
  let bobGovernanceTokenId3;

  before(async () => {
    [alice, bob] = await ethers.getSigners();

    const LinearGitDao = await ethers.getContractFactory("LinearGitDao", alice);
    contractFromAlice = await LinearGitDao.deploy(
        "test", // _projectName
        10, // _challengeDurationInSeconds
        20, // _proposalCreatedToVotingOpenedDurationInSecond
        30, // _votingOpenedDurationInSecond
        10
    );
    contractFromBob = await contractFromAlice.connect(bob);

    // Give Alice some governance tokens
    const aliceTokenReceipt = await (await contractFromAlice.mint(alice.address)).wait();
    aliceGovernanceTokenId = aliceTokenReceipt.events[1].args["id"];

    const aliceTokenReceipt2 = await (await contractFromAlice.mint(alice.address)).wait();
    aliceGovernanceTokenId2 = aliceTokenReceipt2.events[1].args["id"];

    const aliceTokenReceipt3 = await (await contractFromAlice.mint(alice.address)).wait();
    aliceGovernanceTokenId3 = aliceTokenReceipt3.events[1].args["id"];

    // Give Bob some governance tokens
    const bobTokenReceipt = await (await contractFromAlice.mint(bob.address)).wait();
    bobGovernanceTokenId = bobTokenReceipt.events[1].args["id"];

    const bobTokenReceipt2 = await (await contractFromAlice.mint(bob.address)).wait();
    bobGovernanceTokenId2 = bobTokenReceipt2.events[1].args["id"];

    const bobTokenReceipt3 = await (await contractFromAlice.mint(bob.address)).wait();
    bobGovernanceTokenId3 = bobTokenReceipt3.events[1].args["id"];
  });

  // Values of the created proposal
  let proposalReceipt;
  let proposalId;
  let proposalProposer
  let proposalCreationInstant;
  let proposalCommitHash = "0x1234567890";
  let proposalVotingStartInstant;
  let proposalVotingStopInstant;

  beforeEach(async () => {
    proposalReceipt = await (await contractFromAlice.createProposal(proposalCommitHash)).wait();
    
    proposalId = proposalReceipt.events[0].args[0];
    proposalProposer = proposalReceipt.events[0].args[1];
    proposalCreationInstant = BigNumber.from(proposalReceipt.events[0].args[2]);
    proposalCommitHash = proposalReceipt.events[0].args[3];
    proposalVotingStartInstant = proposalReceipt.events[0].args[4];
    proposalVotingStopInstant = proposalReceipt.events[0].args[5];
  });

  function checkAllPossibleAction(
    canCancelProposal,
    canCastChallengeVote,
    canCastVote,
    canChallengeProposal,
    canFinishChallenge,
    canFinishProposal
  ) {
    if (canCancelProposal) {
      it("should allow canceling a proposal", async () => {
        let cancelReceipt = await (await contractFromAlice.cancelProposal(proposalId)).wait();

        expect(cancelReceipt.events[0].event).to.equal("ProposalCanceled");
        expect(cancelReceipt.events[0].args[0]).to.equal(proposalId);
      });

      it("should revert when non-proposer tries to cancel", async () => {
        await expect(
          contractFromBob.cancelProposal(proposalId)
        ).to.be.revertedWith("GitDao: Proposal cannot be canceled.");
      });
    } else {
      it("should not allow canceling a proposal", async () => {
        await expect(
          contractFromAlice.cancelProposal(proposalId)
        ).to.be.revertedWith("GitDao: Proposal cannot be canceled.");
      });
    }

    if (canCastChallengeVote) {
      it("should allow proposer to cast challenge votes", async () => {
        let receipt = await (await contractFromAlice.castChallengeVote(
          proposalId,
          [aliceGovernanceTokenId],
          true
        )).wait();

        expect(receipt.events[0].event).to.equal("ChallengeVoteCast");
        expect(receipt.events[0].args[0]).to.equal(alice.address);
        expect(receipt.events[0].args[1]).to.equal(proposalId);
        expect(receipt.events[0].args[2]).to.deep.equal([aliceGovernanceTokenId]);
        expect(receipt.events[0].args[3]).to.be.equal(100);
        expect(receipt.events[0].args[4]).to.be.equal(true);
      });
      it("should allow non-proposer to cast challenge votes", async () => {
        let receipt = await (await contractFromBob.castChallengeVote(
          proposalId,
          [bobGovernanceTokenId],
          false
        )).wait();

        expect(receipt.events[0].event).to.equal("ChallengeVoteCast");
        expect(receipt.events[0].args[0]).to.equal(bob.address);
        expect(receipt.events[0].args[1]).to.equal(proposalId);
        expect(receipt.events[0].args[2]).to.deep.equal([bobGovernanceTokenId]);
        expect(receipt.events[0].args[3]).to.be.equal(100);
        expect(receipt.events[0].args[4]).to.be.equal(false);
      });
      it("should not allow people to use token they do not own while casting challenge votes", async () => {
        await expect(
          contractFromBob.castChallengeVote(proposalId, [aliceGovernanceTokenId], true)
        ).to.be.revertedWith(
          "NonTransferableERC721: Only the owner of the token is allowed to perform this action"
        );
      });
      it("should allow people to use multiple token while casting votes", async () => {
        let receipt = await (await contractFromBob.castChallengeVote(
          proposalId,
          [bobGovernanceTokenId, bobGovernanceTokenId2],
          false
        )).wait();

        expect(receipt.events[0].event).to.equal("ChallengeVoteCast");
        expect(receipt.events[0].args[0]).to.equal(bob.address);
        expect(receipt.events[0].args[1]).to.equal(proposalId);
        expect(receipt.events[0].args[2]).to.deep.equal(
          [bobGovernanceTokenId, bobGovernanceTokenId2]
        );
        expect(receipt.events[0].args[3]).to.be.equal(200);
        expect(receipt.events[0].args[4]).to.be.equal(false);
      });
      it("should not allow people to use again the tokens they already used", async () => {
        await (await contractFromBob.castChallengeVote(
          proposalId,
          [bobGovernanceTokenId],
          false
        )).wait();

        await expect(
          contractFromBob.castChallengeVote(proposalId, [bobGovernanceTokenId], true)
        ).to.be.revertedWith("GitDao: token was already used to vote on this challenge");
      });
      it("should not allow people to use non-existent tokens", async () => {
        await expect(
          contractFromBob.castChallengeVote(proposalId, [123456], true)
        ).to.be.revertedWith("NonTransferableERC721: Query for non-existing token");
      });
    } else {
      it("should not allow casting challenge votes", async () => {
        await expect(
          contractFromBob.castChallengeVote(proposalId, [bobGovernanceTokenId], true)
        ).to.be.revertedWith("GitDao: Cannot vote on challenge.");
      });
    }

    if (canCastVote) {
      it("should allow proposer to cast votes", async () => {
        let receipt = await (await contractFromAlice.castVote(
          proposalId,
          [aliceGovernanceTokenId],
          true
        )).wait();

        expect(receipt.events[0].event).to.equal("VoteCast");
        expect(receipt.events[0].args[0]).to.equal(alice.address);
        expect(receipt.events[0].args[1]).to.equal(proposalId);
        expect(receipt.events[0].args[2]).to.deep.equal([aliceGovernanceTokenId]);
        expect(receipt.events[0].args[3]).to.be.equal(100);
        expect(receipt.events[0].args[4]).to.be.equal(true);
      });
      it("should allow non-proposer to cast votes", async () => {
        let receipt = await (await contractFromBob.castVote(
          proposalId,
          [bobGovernanceTokenId],
          false
        )).wait();

        expect(receipt.events[0].event).to.equal("VoteCast");
        expect(receipt.events[0].args[0]).to.equal(bob.address);
        expect(receipt.events[0].args[1]).to.equal(proposalId);
        expect(receipt.events[0].args[2]).to.deep.equal([bobGovernanceTokenId]);
        expect(receipt.events[0].args[3]).to.be.equal(100);
        expect(receipt.events[0].args[4]).to.be.equal(false);
      });
      it("should not allow people to use token they do not own while casting votes", async () => {
        await expect(
          contractFromBob.castVote(proposalId, [aliceGovernanceTokenId], true)
        ).to.be.revertedWith(
          "NonTransferableERC721: Only the owner of the token is allowed to perform this action"
        );
      });
      it("should allow people to use multiple token while casting votes", async () => {
        let receipt = await (await contractFromBob.castVote(
          proposalId,
          [bobGovernanceTokenId, bobGovernanceTokenId2, bobGovernanceTokenId3],
          false
        )).wait();

        expect(receipt.events[0].event).to.equal("VoteCast");
        expect(receipt.events[0].args[0]).to.equal(bob.address);
        expect(receipt.events[0].args[1]).to.equal(proposalId);
        expect(receipt.events[0].args[2]).to.deep.equal(
          [bobGovernanceTokenId, bobGovernanceTokenId2, bobGovernanceTokenId3]
        );
        expect(receipt.events[0].args[3]).to.be.equal(300);
        expect(receipt.events[0].args[4]).to.be.equal(false);
      });
      it("should not allow people to use again the tokens they already used", async () => {
        await (await contractFromBob.castVote(
          proposalId,
          [bobGovernanceTokenId],
          false
        )).wait();

        await expect(
          contractFromBob.castVote(proposalId, [bobGovernanceTokenId], true)
        ).to.be.revertedWith("GitDao: token was already used to vote on this proposal");
      });
      it("should not allow people to use non-existent tokens", async () => {
        await expect(
          contractFromBob.castVote(proposalId, [123456], true)
        ).to.be.revertedWith("NonTransferableERC721: Query for non-existing token");
      });
    } else {
      it("should not allow casting votes", async () => {
        await expect(
          contractFromBob.castVote(proposalId, [bobGovernanceTokenId], true)
        ).to.be.reverted;
      });
    }

    if (canChallengeProposal) {
      it("should allow challenging a proposal", async () => {
        let receipt = await (await contractFromAlice.challengeProposal(proposalId, [aliceGovernanceTokenId])).wait();

        expect(receipt.events[0].event).to.equal("ProposalChallenged");
        expect(receipt.events[0].args[0]).to.equal(proposalId);
        expect(receipt.events[0].args[1]).to.equal(alice.address);
        expect(receipt.events[0].args[2].length).to.equal(1);
        expect(receipt.events[0].args[2][0]).to.equal(aliceGovernanceTokenId);
        let challengeStartMoment = receipt.events[0].args[3];
        expect(receipt.events[0].args[4]).to.equal(challengeStartMoment.add(10));
      });
    } else {
      it("should not allow challenging a proposal", async () => {
        await expect(
          contractFromAlice.challengeProposal(proposalId, [aliceGovernanceTokenId])
        ).to.be.revertedWith("GitDao: Proposal cannot be challenged.");
      })
    }

    if (canFinishChallenge) {
      it("should allow the proposer to finish a challenge", async () => {
        let receipt = await (await contractFromAlice.finishChallenge(proposalId)).wait();

        expect(receipt.events[1].event).to.be.oneOf(["ProposalWhitelisted", "ProposalBlacklisted"]);
        expect(receipt.events[1].args[0]).to.equal(proposalId);
      });
      it("should allow non-proposer to finish a proposal", async () => {
        let receipt = await (await contractFromBob.finishChallenge(proposalId)).wait();

        expect(receipt.events[1].event).to.be.oneOf(["ProposalWhitelisted", "ProposalBlacklisted"]);
        expect(receipt.events[1].args[0]).to.equal(proposalId);
      });
    } else {
      it("should not allow finishing challenge", async () => {
        await expect(
          contractFromBob.finishChallenge(proposalId)
        ).to.be.revertedWith("GitDao: Challenge cannot be finished");
      });
    }

    if (canFinishProposal) {
      it("should allow the proposer to finish a proposal", async () => {
        let receipt = await (await contractFromAlice.finishProposal(proposalId)).wait();

        expect(receipt.events[0].event).to.be.oneOf(["ProposalSucceeded", "ProposalFailed"]);
        expect(receipt.events[0].args[0]).to.equal(proposalId);
      });
      it("should allow non-proposer to finish a proposal", async () => {
        let receipt = await (await contractFromBob.finishProposal(proposalId)).wait();

        expect(receipt.events[0].event).to.be.oneOf(["ProposalSucceeded", "ProposalFailed"]);
        expect(receipt.events[0].args[0]).to.equal(proposalId);
      });
    } else {
      it("should not allow finishing the proposal", async () => {
        await expect(contractFromBob.finishProposal(proposalId)).to.be.revertedWith(
          "GitDao: Proposal cannot be finished"
        );
      });
    }
  }

  it("should allow the creation of proposals", async () => {
    expect(proposalReceipt.events[0].event).to.equal("ProposalCreated");
    expect(proposalId).to.equal(1);
    expect(proposalProposer).to.equal(alice.address);
    expect(proposalCommitHash).to.equal(proposalCommitHash);
    expect(proposalVotingStartInstant).to.equal(proposalCreationInstant.add(20));
    expect(proposalVotingStopInstant).to.equal(proposalCreationInstant.add(20).add(30));
  });

  describe("Pending", () => {
    checkAllPossibleAction(
      canCancelProposal = true,
      canCastChallengeVote = false,
      canCastVote = false,
      canChallengeProposal = true,
      canFinishChallenge = false,
      canFinishProposal = false
    )
  });

  describe("Canceled", () => {

    beforeEach(async () => {
      await (await contractFromAlice.cancelProposal(proposalId)).wait();
    });

    checkAllPossibleAction(
      canCancelProposal = false,
      canCastChallengeVote = false,
      canCastVote = false,
      canChallengeProposal = false,
      canFinishChallenge = false,
      canFinishProposal = false
    )
  });

  describe("Voting Opened", () => {

    beforeEach(async () => {
      // Wait a bit for voting to open
      await network.provider.send("evm_increaseTime", [21])
    });

    checkAllPossibleAction(
      canCancelProposal = false,
      canCastChallengeVote = false,
      canCastVote = true,
      canChallengeProposal = true,
      canFinishChallenge = false,
      canFinishProposal = false
    )
  });

  describe("Voting Elapsed", () => {

    beforeEach(async () => {
      // Wait a bit for voting to open
      await network.provider.send("evm_increaseTime", [51])
    });

    checkAllPossibleAction(
      canCancelProposal = false,
      canCastChallengeVote = false,
      canCastVote = false,
      canChallengeProposal = true,
      canFinishChallenge = false,
      canFinishProposal = true
    )
  });

  describe("Succeeded", () => {

    beforeEach(async () => {
      // Wait a bit for voting to be elapsed
      await network.provider.send("evm_increaseTime", [51])
      // Finish the proposal
      await (await contractFromAlice.finishProposal(proposalId)).wait();
    });

    checkAllPossibleAction(
      canCancelProposal = false,
      canCastChallengeVote = false,
      canCastVote = false,
      canChallengeProposal = false,
      canFinishChallenge = false,
      canFinishProposal = false
    )
  });

  describe("Deafeated", () => {

    beforeEach(async () => {
      // Wait a bit for voting to open
      await network.provider.send("evm_increaseTime", [21])

      // Negative vote to defeat the proposal
      contractFromAlice.castVote(
        proposalId,
        [aliceGovernanceTokenId],
        false
      )
      // Wait a bit for voting to b elapsed
      await network.provider.send("evm_increaseTime", [30])

      // Finish the proposal
      await (await contractFromAlice.finishProposal(proposalId)).wait();
    });

    checkAllPossibleAction(
      canCancelProposal = false,
      canCastChallengeVote = false,
      canCastVote = false,
      canChallengeProposal = false,
      canFinishChallenge = false,
      canFinishProposal = false
    )
  });

  describe("Challenged", () => {

    beforeEach(async () => {
      // Tokens to minimum possible value
      await network.provider.send("evm_increaseTime", [10])

      await (await contractFromBob.challengeProposal(proposalId, [bobGovernanceTokenId3])).wait();
    });

    checkAllPossibleAction(
      canCancelProposal = false,
      canCastChallengeVote = true,
      canCastVote = false,
      canChallengeProposal = false,
      canFinishChallenge = false,
      canFinishProposal = false
    )
  });

  describe("Challenge Elapsed", () => {

    beforeEach(async () => {

      const aliceTokenReceipt4 = await (await contractFromAlice.mint(alice.address)).wait();
      aliceGovernanceTokenId4 = aliceTokenReceipt4.events[1].args["id"];

      // Tokens to minimum possible value
      await network.provider.send("evm_increaseTime", [10])

      await (await contractFromAlice.challengeProposal(proposalId, [aliceGovernanceTokenId4])).wait();
      await (await contractFromAlice.castChallengeVote(proposalId, [aliceGovernanceTokenId3], false)).wait();
      await (await contractFromBob.castChallengeVote(proposalId, [bobGovernanceTokenId3], false)).wait();

      await network.provider.send("evm_increaseTime", [11])
    });

    checkAllPossibleAction(
      canCancelProposal = false,
      canCastChallengeVote = false,
      canCastVote = false,
      canChallengeProposal = false,
      canFinishChallenge = true,
      canFinishProposal = false
    )
  });
});
